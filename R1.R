#导入包

library(ggplot2)
library(reshap2)
library(plyr)

#准备数据

vehicles <- read.csv(unz("vehicles.csv.zip","vehicles.csv"),stringsAsFactors = F)
x <- readLines("varlabels.txt")
y <- strsplit(x,"-")
labels <- do.call(rbind,y)

colors <- c('green','red','yellow','blue')
colors_factors <- factor(colors)
colors_factors
#当我们将数据导入R时，我们经常会遇到某一列明明是数值类型的，但是其中却夹杂着非数值的情况。
#此时R可能将这列当做factor来导入

#探索与描述数据

nrow(vehicles)
ncol(vehicles)
names(vehicles)
length(unique(vehicles[,"year"]))
first_year <- min(vehicles[,"year"])
last_year <- max(vehicles[,"year"])
length(unique(vehicles$year))
table(vehicles$fuelType1)

vehicles$trany[vehicles$trany == ""] <- NA
vehicles$trany2 <- ifelse(substr(vehicles$trany,1,4)== "Auto","Auto","Manual")

vehicles$trany <- as.factor(vehicles$trany)
table(vehicles$trany2)

with(vehicles,table(sCharger,year))

class(vehicles$sCharger)
unique(vehicles$sCharger)

class(vehicles$tCharger)
unique(vehicles$tCharger)


#按年份整合，然后对每个组计算highway,city,combine的燃油效率，将这个结果赋值给一个新的数据框
mpgByYr <- ddply(vehicles,~year,summarise,
                 avgMPG = mean(comb08),
                 avgHghy = mean(highway08),
                 avgCity = mean(city08))

#用散点图回执avgMPG与year之间的关系，其中需要坐标轴的命名，图的标题，一级加上一个平滑的条件均值，geom_month()在图片上增加一个阴影区域
ggplot(mpgByYr,aes(year,avgMPG)) + geom_point() + geom_smooth() + xlab("Year") + ylab("Average MPG") + ggtitle("All cars")

table(vehicles$fuelType1)
#使用subset函数来shshangchen生成一个新的数据框
gasCars <- subset(vehicles,fuelType1 %in% c("RagularGasoline","Premium Gasoline","Midgrade Gasolin") & 
                    fuelType2 == "" & atvType != "Hybrid")

mpgByYr_Gas <- ddply(gasCars, ~year,summarise,
                     avgMPG = mean(comb08))

ggplot(mpgByYr_Gas,aes(year,avgMPG)) + geom_point() + geom_smooth() + xlab("Year") + ylab("Average MPG") + ggtitle("Gasoline cars")

#是否是近年来大马力的车产量降低了，如果是，就可以解释这种增长，将字符串类型转化为数值类型
typeof(gasCars$displ)
gasCars$displ <- as.numeric(gasCars$displ)
ggplot(gasCars,aes(displ,comb08)) + geom_point() + geom_smooth()

avgCarSize <- ddply(gasCars,~year,summarise, avgDispl = mean(displ))
ggplot(avgCarSize,aes(year,avgDispl)) + geom_point() + geom_smooth() + xlab("Year") + ylab("Average engine displacement(1)")

#使用ddlpy函数生成一个新的数据框

byYear <- ddply(gasCars,~year,summarise, avgMPG= mean(comb08), avgDispl = mean(displ))
head(byYear)

#使用ggplot2包，在同一张图但是不同的面上来逐年显示平均油耗及平均排量之间的关系
#分解数据框，将一个宽的数据框分解为一个长的数据框

byYear2 <- melt(byYear,id = "year")
levels(byYear2$variable) <- c("Average MPG", "Avg engine displacement")
head(byYear2)

ggplot(byYear2,aes(year,value)) + geom_point() + geom_smooth() + facet_wrap(~variable, nco = 1,scales = "free_y") + xlab("YEar") + ylab("")

#在小排量的引擎上，看看是否有自动挡或者手动挡传动比四缸发动的油耗更高效
#以及油耗是如何随时间变化的

gasCars4 <- subset(gasCars,cylinders == "4")
ggplot(gasCars4,aes(factor(year),comb08)) + geom_boxplot() + facet_wrap(~trany2, ncol = 1) + theme(axis.text.x =  element_text(angle = 45)) + labs(x="Year",y="MPG")

#每一年手动挡的车的占比情况

ggplot(gasCars4,aes(factor(year), fill = factor(trany2))) + geom_bar(position = "fill") + labs(x = "Year", y = "Proportion of cars" , fill = "Transmission" ) + theme(axis.title.x = element_text(angle = 45)) + geom_hline(yintercept = 0.5,linetype = 2)

#研究汽车的产量以及车型

#汽车的生产商和型号是如何随时间改变的
carsMake <- ddply(gasCars4,~year,summarise,numberOfMakes = length(unique(make)))

ggplot(carsMake,aes(year,numberOfMakes)) + geom_point() + labs(x = "Year", y = "Number of avalible makes") + ggtitle("four cylinder cars")

uniqMakes <- dlply(gasCars4, ~year, function(x) unique(x$make))
commonMakes <- Reduce(intersect, uniqMakes)
commonMakes

#汽车燃油效率
carsCommonMakes <- subset(gasCars4,make %in% commonMakes)
avgMPG_commonMakes <- ddply(carsCommonMakes4, ~year + make, summarise, avgMPG = mean(comb08))
ggplot(avgMPG_commonMakes,aes(year,avgMPG)) + geom_line() + facet_wrap(~make, nrow = 3)


